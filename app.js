//app.js
import API from './Api/Api.js'
import URL from './Api/url.js'
App({
  onLaunch: function() {
    const that = this
    wx.getSystemInfo({
      success: function(res) {
        that.globalData.sysInfo = res
      },
    })
  },
  URL: URL,
  Api: API,
  globalData: {
    sysInfo: {}
  }
})