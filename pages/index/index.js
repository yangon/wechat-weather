var QQMapWX = require('../../utils/map/qqmap-wx-jssdk.js');
var qqmapsdk;
var app = new getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    safeArea: {},
    fH: 44,
    location: {
      city: '北京'
    },
    now_weather: {},
    dayTime: null,
    suggestion: null,
    onInitChart: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '定位中...',
    })
    qqmapsdk = new QQMapWX({
      key: 'IP6BZ-PVTWW-LWVR3-RPGDJ-MVFEF-4NFAK'
    });
    console.log(app.globalData.sysInfo)
    this.setData({ fH: app.globalData.sysInfo.statusBarHeight + app.globalData.sysInfo.safeArea.top + 20})
    wx.getLocation({
      success: (res) => {
        console.log(res)
        wx.hideLoading()
        qqmapsdk.reverseGeocoder({
          location: {
            latitude: res.latitude,
            longitude: res.longitude
          },
          success: (r) => {
            // this.setData({ location: r.result.ad_info})
            this.getNow()
            this.getSun()
            this.getSuggestion()
            this.initChart()
            this.getDaily()
          },
          fail: (e) => {
            this.setData({ location: { city: '北京' } })
            this.getNow()
            this.getSun()
            this.getSuggestion()
            this.initChart()
            this.getDaily()
          }
        })
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '位置信息获取失败',
        })
        this.setData({location: {city: '北京'}})
        this.getNow()
        this.getSun()
        this.getSuggestion()
        this.initChart()
        this.getDaily()
      }
    })
  },
  initChart () {
    var onInitChart = (F2, config) => {
      const chart = new F2.Chart(config);
      const data = [];
      chart.source(data, {
        time: {
          type: 'timeCat',
          tickCount: 3,
          mask: 'hh:mm',
          range: [0, 1]
        },
        value: {
          tickCount: 3,
          formatter: function formatter(ivalue) {
            return ivalue + '%';
          }
        }
      });
      chart.axis('time', {
        line: null,
        label: function label(text, index, total) {
          const textCfg = {};
          if (index === 0) {
            textCfg.textAlign = 'left';
          } else if (index === total - 1) {
            textCfg.textAlign = 'right';
          }
          return textCfg;
        }
      });
      chart.axis('tem', {
        grid: function grid(text) {
          if (text === '0%') {
            return {
              lineDash: null,
              lineWidth: 1
            };
          }
        }
      });
      chart.line()
        .position('time*value')
        .color('type')
        .shape('type', function (type) {
          if (type === '预期收益率') {
            return 'line';
          }
          if (type === '实际收益率') {
            return 'dash';
          }
        });
      chart.render();
      return chart;
    }
    this.setData({ onInitChart: onInitChart})
  },
  getNow () {
    app.Api._get(app.URL.weather, { location: this.data.location.city}).then((res) => {
      this.setData({ now_weather: res.results[0].now })
    })
  },
  getSun () {
    app.Api._get(app.URL.sun, { location: this.data.location.city, start: 1, days: 1 }).then((res) => {
      this.setData({ dayTime: res.results[0].sun[0]})
    })
  },
  getSuggestion () {
    app.Api._get(app.URL.suggestion, { location: this.data.location.city}).then((res) => {
      this.setData({ suggestion: res.results[0].suggestion})
      wx.stopPullDownRefresh()
    })
  },
  getDaily() {
    app.Api._get(app.URL.daily, { location: this.data.location.city }).then((res) => {
      this.setData({ daily: res.results[0].daily })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getNow()
    this.getSun()
    this.getSuggestion()
    this.initChart()
    this.getDaily()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})