const baseUrl = 'https://api.seniverse.com/v3/'
export default {
  weather: baseUrl + 'weather/now.json',
  sun: baseUrl + 'geo/sun.json',
  suggestion: baseUrl + 'life/suggestion.json',
  daily: baseUrl + 'weather/daily.json'
}