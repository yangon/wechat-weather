var baseUrl = 'https://api.seniverse.com/v3/weather/'
const http = ({
  url = '',
  param = {},
  method = 'get',
  ...other
} = {}) => {
  let timeStart = Date.now();
  let headers = {
    'content-type': 'application/json',
    ...other.header
  }
  param.key = 'SrV8doEZlcLJrFi9M'
  return new Promise((resolve, reject) => {
    wx.request({
      url: url,
      data: param,
      method: method,
      header: headers,
      complete: (res) => {
        console.log(`耗时${Date.now() - timeStart}`);
        if (res.statusCode >= 200 && res.statusCode < 300) {
          // 正常请求
          resolve(res.data)
        } else {
          console.log(res.statusCode)
          // 数据异常处理
          reject(res)
        }
      }
    })
  })
}

// get方法
const _get = (url, param = {}, other = {}) => {
  return http({
    url,
    param,
    method: 'get',
    ...other
  })
}

const _post = (url, param = {}, other = {}) => {
  return http({
    url,
    param,
    method: 'post',
    ...other
  })
}

const _put = (url, param = {}, other = {}) => {
  return http({
    url,
    param,
    method: 'put',
    ...other
  })
}

const _delete = (url, param = {}, other = {}) => {
  return http({
    url,
    param,
    method: 'put',
    ...other
  })
}
module.exports = {
  _get,
  _post,
  _put,
  _delete
}